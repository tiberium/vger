import requests
session = requests.Session()

# Wenn benötigt, auf True setzen und lege unsere Zugangsdaten in die Session mir rein. README lesen!
# if False:
#     jar = requests.cookies.RequestsCookieJar()
#     jar.set('wcf21_userID','123')
#     jar.set('wcf21_password','Buchstabensalat')
#     jar.set('wcf21_cookieHash','BuchstabensalatAberMitMehrZahlen')
#     session.cookies = jar

if True:
    jar = requests.cookies.RequestsCookieJar()
    jar.set('wcf21_userID','194')
    jar.set('wcf21_password','$2a$08$gzF1vMvv.TvJtRy.wukg7Oz4FuzAtJJRMTenFvP2JJBejJyMVWBKC')
    jar.set('wcf21_cookieHash','699aaeab91909fbb2a7a8d2c455e0790d06673b9')
    session.cookies = jar


# Alle Urls hier hinein, die Liste kann beliebig nach dem Schema erweitert werden.
# Bitte Beispiele aus der Liste entfernen und letzte Url ohne Komma und Backslash!
# all_urls = ["https://adresse.de/board/index.php?thread/xxxxxx/",\
#     "weitere URL",\
#     "weitere URL"]

# Alle Urls hier hinein, die Liste kann beliebig nach dem Schema erweitert werden.
# Bitte Beispiele aus der Liste entfernen und letzte Url ohne Komma und Backslash!
all_urls = ["https://trekzone.de/sfrs/board/index.php?thread/7115-season-30-bordleben/", \
  "https://trekzone.de/sfrs/board/index.php?thread/7114-missionsleitung-2020-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/462-hall-of-fame-der-uss-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/807-schiffsreihe-der-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/61-abwesenheit-der-vernecrew/", \
  "https://trekzone.de/sfrs/board/index.php?thread/60-verne-off-play/", \
  "https://trekzone.de/sfrs/board/index.php?thread/59-logbuch-der-uss-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/7118-pers%C3%B6nliches-logbuch-t%E2%80%98jul/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6882-pers%C3%B6nliches-logbuch-glia-nightingale/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5668-pers%C3%B6nliches-logbuch-sokur/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1134-pers%C3%B6nliches-logbuch-tiberium-vouk/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1240-eingehende-transmissionen-des-sternenflottenkommandos/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5291-eingehende-transmissionen-des-sternenflottenkonstruktionskommandos/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1465-eingehende-transmissionen-des-sternenflottengeheimdienst/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6743-eingehende-transmissionen-von-der-uss-swansea/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4979-subraumnachrichten-von-der-uss-fenrir/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5025-subraumnachrichten-von-der-uss-esquiline/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3364-sonstige-subraumnachrichten/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3750-subraumnachrichten-von-der-uss-aurora/", \
  "https://trekzone.de/sfrs/board/index.php?thread/38-subraumnachrichten-von-der-uss-pandora/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6845-missionsleitung-2020-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6747-missionsleitung-2018-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6702-missionsleitung-2018-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6682-missionsleitung-2017-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6126-missionsleitung-2014-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6011-missionsleitung-2013-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5704-missionsleitungen-2012-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5599-missionsleitungen-2012-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5151-missionsleitungen-2011-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4897-missionsleitungen-2011-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4604-missionsleitungen-2010-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4522-boardlebenplanungen-2010/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4336-missionsleitung-2010-januar-bis-juni-season-9/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6927-season-27-29-bordleben/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6618-season-23-bordleben-2-psychische-belastungen/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6730-season-26-bordleben-1-die-kolonie/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6681-missionsleitung-2017-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6680-missionsleitung-2016-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6582-season-23-bordleben-1-au%C3%9Fer-kontrolle/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6529-missionsleitung-2016-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6128-season-17-missionbl-1/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6127-season-17-bordleben-1-its-something-big/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6396-missionsleitung-2015-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/6053-season-16-bordleben-2-the-last-dance-fowl/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5998-season-16-bordleben-1-the-last-dance-meat/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4457-veraltet-wiki-der-uss-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5964-season-15-bordleben-2-out-of-hell-athanassios/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5987-pers%C3%B6nliches-logbuch-bryan-mcmillan/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5827-season-15-bordleben-1-out-of-hell-repelled/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5705-season-14-bordleben-1-und-2-back-to-hell-confuse/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5421-season-13-bordleben-1-und-2-back-to-hell-orientation/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5163-season-12-bordleben-1-invisible-enemy-radiation/", \
  "https://trekzone.de/sfrs/board/index.php?thread/5042-season-11-bordleben-2-status-road-to-insights/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4928-season-11-bordleben-1-status-road-to-ruin/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4603-season-10-bordleben-1-reflection-into-the-sun/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3701-subraumnachrichten-von-der-uss-troy/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4428-season-9-bordleben-1-rekonstruktion-into-the-distant/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4281-season-9-bordleben-1-rekonstruktion-coming-home/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4281-season-9-bordleben-1-rekonstruktion-coming-home/", \
  "https://trekzone.de/sfrs/board/index.php?thread/46-subraumnachrichten-von-der-uss-ares/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4209-season-8-bordleben-2-das-konsortium-das-leben-in-der-h%C3%B6llendimension/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4017-season-8-bordleben-1-das-konsortium-willkommen-in-der-h%C3%B6llendimension/", \
  "https://trekzone.de/sfrs/board/index.php?thread/4002-season-8-prelog-ds4/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3721-season-7-bordleben-2-come-alive-sp%C3%BCre-dass-du-lebst/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3878-abschied/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3614-season-7-bordleben-1-come-alive-neue-konstellationen/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2026-pers%C3%B6nliches-logbuch-john-ice/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3447-season-6-bordleben-2-home-sweet-home-ein-schritt-dem-weg-entgegen/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3527-season-7-prelog-ds4/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3537-missionsleitungen-2009-i-und-ii-quartal-season-7/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3332-season-6-bordleben-1-home-sweet-home-eingew%C3%B6hnung/", \
  "https://trekzone.de/sfrs/board/index.php?thread/45-subraumnachrichten-uss-rhode-island/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2863-season-5-bordleben-2-unending-uss-persephone/", \
  "https://trekzone.de/sfrs/board/index.php?thread/3240-missionsleitungen-2008-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2782-season-5-bordleben-1-unending-die-geschichte-eines-schiffes/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2549-season-4-bordleben-2-hin-und-zur%C3%BCck-die-geschichte-eines-volkes/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2755-missionsleitungen-2008-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/58-personallogbuch-der-uss-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1990-missionsleitungen-2007-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/41-subraumnachrichten-uss-avalon/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2386-season-4-bordleben-1-hin-und-zur%C3%BCck-die-geschichte-einer-suche/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2182-status-der-deep-space-mission-pegasus/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2064-sinn-oder-unsinn/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1989-missionsleitungen-2007-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2462-dsm/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2278-season-3-bordleben-2-al-principio-fue-el-fin/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2316-missionslog-mne-ah-terrh-ch-rihan/", \
  "https://trekzone.de/sfrs/board/index.php?thread/2156-season-3-bordleben-1-maisha-marefu-matamu-sana/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1810-season-2-bordleben-16-inter-spem-et-metum/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1614-missionsleitungen-2006-iii-und-iv-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/53-pers%C3%B6nliches-logbuch-siw-istad/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1574-season-2-bordleben-15-sag-zum-abschied-leise-danke/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1524-pers%C3%B6nliches-logbuch-elia/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1167-missionsleitungen-2006-i-und-ii-quartal/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1219-season-1-bordleben-14-auf-ins-unentdeckte-land/", \
  "https://trekzone.de/sfrs/board/index.php?thread/599-technische-daten-der-u-s-s-verne/", \
  "https://trekzone.de/sfrs/board/index.php?thread/29-subraumnachrichten-uss-allegiance/xx", \
  "https://trekzone.de/sfrs/board/index.php?thread/1148-bordleben-13-spannungen-und-mehr-auf-den-weg-nach-hause/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1040-pers%C3%B6nliches-logbuch-chris-shark/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1115-bordleben-12-der-weg-zur%C3%BCck/", \
  "https://trekzone.de/sfrs/board/index.php?thread/1061-bordleben-11-ein-weiterer-schritt-zum-ziel/", \
  "https://trekzone.de/sfrs/board/index.php?thread/339-missionsleitungen-2005/", \
  "https://trekzone.de/sfrs/board/index.php?thread/57-pers%C3%B6nliches-logbuch-talea-saris/", \
  "https://trekzone.de/sfrs/board/index.php?thread/989-bordleben-10-das-hier-und-jetzt-z%C3%A4hlt/", \
  "https://trekzone.de/sfrs/board/index.php?thread/47-subraumnachrichten-uss-prevalance/", \
  "https://trekzone.de/sfrs/board/index.php?thread/918-bordleben-9-die-reise-der-hoffnung-geht-weiter/", \
  "https://trekzone.de/sfrs/board/index.php?thread/863-bordleben-8-viele-puzzelteile-ergeben-ein-ganzes/", \
  "https://trekzone.de/sfrs/board/index.php?thread/806-bordleben-7-neue-abenteuer-und-neue-gefahren/", \
  "https://trekzone.de/sfrs/board/index.php?thread/719-bordleben-6-die-hoffnung-stirbt-zuletzt/", \
  "https://trekzone.de/sfrs/board/index.php?thread/642-bordleben-5-wir-stellen-uns-neuen-herausforderungen/", \
  "https://trekzone.de/sfrs/board/index.php?thread/597-bordleben-4-ein-neues-schiff-und-es-geht-weiter/", \
  "https://trekzone.de/sfrs/board/index.php?thread/551-bordleben-3-eine-herausforderung-die-bew%C3%A4ltigt-werden-wird/", \
  "https://trekzone.de/sfrs/board/index.php?thread/541-bordleben-2-der-aufbruch-ins-unbekannte/", \
  "https://trekzone.de/sfrs/board/index.php?thread/62-bordleben1-auf-ein-neues/", \
  "https://trekzone.de/sfrs/board/index.php?thread/56-pers%C3%B6nliches-logbuch-tanot/"]