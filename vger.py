#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Ein Script zum Exportieren von Foren Threads aus einem WBB Forum.
    Jeder Thread wird als eigenständige HTML Datei gespeichert.

    Bitte die Liste all_urls Vervollständigen.

    Sollten ein Foren Thread, in einem nicht öffentlichen Bereich sein, so 
    benötigt das Script noch die drei Anmelde Cookies des Acoounts von WBB.
    Werte bitte in die jar.set Variabeln ablegen. Beachte die README.md dazu.
"""

__author__ = "Denis Schultz"
__copyright__ = "Copyright 2020, Denis Schultz"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Denis Schultz"
__email__ = "tiberiumschniffer@gmail.com"
__status__ = "Production"

import os
import sys
import string
from sys import platform
from pathlib import Path
import requests
from bs4 import BeautifulSoup
session = requests.Session()
from settings import *

# # Alle Urls hier hinein, die Liste kann beliebig nach dem Schema erweitert werden.
# # Bitte Beispiele aus der Liste entfernen und letzte Url ohne Komma und Backslash!
# all_urls = ["https://adresse.de/board/index.php?thread/xxxxxx/",\
#     "weitere URL",\
#     "weitere URL"]




# # Wenn benötigt, auf True setzen und lege unsere Zugangsdaten in die Session mir rein. README lesen!
# if False:
#     jar = requests.cookies.RequestsCookieJar()
#     jar.set('wcf21_userID','123')
#     jar.set('wcf21_password','Buchstabensalat')
#     jar.set('wcf21_cookieHash','BuchstabensalatAberMitMehrZahlen')
#     session.cookies = jar

# Wir schauen erstmal, wo unser Arbeitsverzeichnis ist und erstellen, falls nicht schon vorhanden, den Ordner für die html Dateien 
path = Path.cwd()
html_path = Path.cwd() / 'html'
html_path.mkdir(parents=True, exist_ok=True)

# Ersetze mehre Zeichen eines Strings.
# Quelle: https://thispointer.com/python-how-to-replace-single-or-multiple-characters-in-a-string/

def replaceMultiple(mainString, toBeReplaces, newString):
    # Durschleife alle Zeichen die ersetzt werden sollten
    for elem in toBeReplaces :
        # Überprüfe ob das Ausgeschlossene Zeichen im string ist
        if elem in mainString :
            # Ersetze die Zeichen
            mainString = mainString.replace(elem, newString)
    
    return  mainString 


# Für jede Adresse in all_urls, speicher eine Adresse in die Variable one_url und gehe damit die For-Schleife durch
for one_url in all_urls:
    # Setze die Standard Variabeln wieder zurück
    activ_url = one_url # Die eine Adresse der Liste wird die Aktive für die darunterliegende While-Schleife
    title = '' # Für den Dateinamen des Threads
    old_url = '' # Wird benutzt um innerhalb der While-Schleife die vorherige Seite zwischen zu speichern
    before_old_url = '' # Wird benutzt um innerhalb der While-Schleife die vor-vorherige (^^) Seite zwischen zu speichern
    before_old_url_withOne = '' # Wie oben, aber mit pageNo=1  suffix
    output = '' # Hier wird der gesamte Thread gesammelt und am Ende in einer Datei gespeichert
    next_page = ''
    error = False

    # Für jede Seite, aus der die "one_url" besteht
    while True:
        req = session.get(activ_url)
        soup = BeautifulSoup(req.text, "html.parser")

        if title == '':
            # Wenn das der erste Durchlauf der While-Schleife ist, also Seite 1: dann setze den Titel
            try:
                title = soup.find(class_= 'messageHeadline').h1.string
            except:
                error = True
                print('\n' + '\n' + ">>> ABBRUCH >>> Es werden Zugangsdaten für folgende Url benötigt:")
            else:
                # Wir bereinigen den titel von Sodnerzeichen und anschließend von Leerzeichen
                title = replaceMultiple(title, string.punctuation , "-")
                title = replaceMultiple(title, string.whitespace , "_")

        if activ_url == before_old_url or activ_url == before_old_url_withOne:
            # Ist diese Seite gleich die Seite davor, dann breche ab
            print('Letzte Seite des Threads, gehe zu nächste Adresse.')
            break
        else:
            # Wir zeigen auf, welche (Unter-)Adresse wir gerade durchgehen
            print (activ_url)
            # Alle Beiträge sind in article-Tag umschlossen
            lis = soup.findAll('article')  
            for li in lis:
                output += str(li)

            # Finde den letzten Weiter (Skip) Button
            # Zuerst schaue, ob das Thema überhaupt mehrseitig ist
            test_next_page = soup.findAll("li", {"class": "button skip"})
            if test_next_page:
                next_page = soup.findAll("li", {"class": "button skip"})[-1]
            else:
                break

            if next_page != '':
                # Ziehe die Link-Adresse aus dem Button
                next_url = next_page.find("a", href=True)

                if next_url:
                    # Kopie zur Überprüfung anlegen, ob wir uns nicht imm Kreis drehen, da auf der letzten Seite
                    # der letzte Button nicht ein Weiter Button ist, sondern ein Zurück Button
                    before_old_url = old_url
                    before_old_url_withOne = before_old_url + '&pageNo=1'
                    old_url = activ_url


                    # Leider werden an der Adresse unnötige Hashs angehängt, die die Adresse unbrauchbar machen
                    # Wir splitten das ganze auf - In Drei Teilen...
                    split_string = next_url['href'].split("&")

                    # Wir setzen Teil 1 und 2 wieder zusammen und verwerfen Teil 3
                    activ_url = split_string[0] + '&' + split_string[1]
                    
                else:
                    break
            else:
                break

    if error == True:
        break
    # Zwischenschritt für Windows. Hier bauen wir den richtige Pfad und Name der Datei, wandeln diese in einen string um und
    # geben ihm anschließend die passende Dateiendung
    data_path = str(Path(html_path) / title)  + '.html'


    # Wir schreiben die HTML Datei
    with open(data_path, "w", encoding='utf-8') as text_file:
        print(output, file=text_file)
        print("(Datei: " + data_path + " geschrieben.)")