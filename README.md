# V'ger

Ein Python Script zum sammeln und sichern von mehrerene WBB Foren Themen auf einmal. Benannt nach der Star Trek Sonde _V'ger_. Da diese Script geschaffen wurden, um Threads von einem WBB Star Trek Foren RPG zu sichern/exportieren.

Das Script speichert momentan nur die einzelne HTML Blöcke eines Artikel in einem Foren Thema und setzt diese in eine Datei je Thema ab. Es ist angedacht in Zukunft, das die Meta Daten und Inhalte in eine CSV Spalten getrennt gespeichert werden. So das diese auch automatisisert, in ein anderes Forum importiert werden können.

## Vorausetzung

- Installiertes Python, in oder höher als Version 3.6
- BeautifulSoup4
- Requests

# Setup
Einfach das Projekt clonen oder als Zip-Archiv herunterladen (Oben, zweiter Button von rechts) und in ein eigenes Verzeichnis extrahieren.

## BeautifulSoup4 und Requests installieren
Das Verzeichnis des Script in Terminal oder PowerShell öffnen. 

### Linux:
Rechtsklick im Ordner und _In Terminal öffnen_ auswählen und dann folgendes eingeben:

`pip3 install -r requirements.txt`

### Windows:
Bei gedrückte Shift-/Hochstelltaste Rechtsklick im Ordner machen und _PowerShell hier öffnen_ auswählen und dann folgendes eingeben:

`pip install -r requirements.txt`

## Adressen eintragen
Das Script kann beliebige viele Adressen bearbeiten. Die Liste kann beliebig nach dem Schema erweitert werden. **Bitte aber die Beispiele aus der Liste entfernen und bei der letzte Url kein Komma und Backslash mehr setzen.**

Eingegeben werden diese ~ ab Zeile 30:

```python
all_urls = ["https://adresse.de/board/index.php?thread/xxxxxx/",\
    "https://adresse.de/board/index.php?thread/aaaa/",\
    "https://adresse.de/board/index.php?thread/bbbb/"]
```

## Zugangsadaten eintragen
Wenn man Themen sichern möchte, die in einem nicht öffentlichen Bereich sind, dann muss man dem Script seine Zugangsdaten, in Cookie-Form, mitgeben.

Der zugehörige Code Abschnitt befindet sich, nach der Adressliste und sieht so aus:

```python
if False:
    jar = requests.cookies.RequestsCookieJar()
    jar.set('wcf21_userID','123')
    jar.set('wcf21_password','Buchstabensalat')
    jar.set('wcf21_cookieHash','BuchstabensalatAberMitMehrZahlen')
    session.cookies = jar
```
Zuerst ändern wir den _False_ Ausdruck in _True_ ab (Groß- und Kleinschreibung beachten!)

Die Cookie Daten bekommt man aus den Entwickler Konsole des Browsers. Hier am Beispiel von Firefox (Ist bei Chrome analog):

1. Öffne die Seite in Firefox und stelle sicher, das du angemeldet bist.
2. Mache ein Rechtsklick auf der geöffnete Seite und klicke auf _Element untersuchen_ (Nur _Untersuchen_ bei Chrome)
3. Es öffnet sich die Entwicklerkonsole. Es ist der Reiter _Inspektor_ offen. Neben ihm, gibt es weitere Reiter, beachte die Doppelpfeile nach rechts und klicke darauf. Du müsstest den Reiter _Netzwerkanalyse_ (_Netzwerk_ bei Chrome) sehen. Klicke darauf.
4. Mit geöffnete _Netzwerkanalyse_, lade die Seite neu.
5. Klicke auf den ersten Eintrag. Wenn du dich auf der Hauptseite befindest, heist er wie die Url nach der Domän (bsp.: /forum/) oder bei Unterseiten oft _index.phpXXXX_
6. Es sollte sich unten, eine neue Maske öffnen. Wieder mit verschiedenen Reitern. Wähle dort den Reiter Cookies.
7. Kopiere die jeweilige Werte in den jeweiligen Hochkommas im Code Block.

Beispiel:
```python
if True:
    jar = requests.cookies.RequestsCookieJar()
    jar.set('wcf21_userID','123')
    jar.set('wcf21_password','$4b$08$gzF1vMvv.TgJtRy.wdkg3Oz4FuzAtJJRqTenFvP2JJBvjJyMVWBKC')
    jar.set('wcf21_cookieHash','b354e2057cb739272135dvc7f367416vf9e6fb52')
    session.cookies = jar
```
## Ausführen
Das Script kann einfach im Verzeichnis gestartet werden. Es empfielt sich wegen der Ausgabe, es vom Termminal oder der PowerShell zu starten.

### Linux:
Rechtsklick im Ordner und _In Terminal öffnen_ auswählen und dann folgendes eingeben:

`python3 vger.py`

### Windows:
Bei gedrückte Shift-/Hochstelltaste Rechtsklick im Ordner machen und _PowerShell hier öffnen_ auswählen. dann folgendes eingeben:

`python vger.py`